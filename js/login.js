$("#formData").submit(function (event) {
  event.preventDefault();
  username=$("#inputEmail").val();
  password=$("#inputPassword").val();
  $.ajax({
    dataType: "json",
    async: false,
    type: "POST",
    url: "../lib/check_login.php",
    data: {username: username, pwd:password},
    success: function(response) {
      if (response.success) {
        data = response.data;
        if (data !== null) {
          window.location=response.location;
        } else {
          swal({
            title: "Error!",
            type: 'error',
            text: response.msg
          })
        }
      } else {
        swal({
          title: "Error!",
          type: 'error',
          text: response.msg
        })
      }
    },
  });
});

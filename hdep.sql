/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     08-06-2019 19:31:04                          */
/*==============================================================*/


drop index AYUDA2_FK;

drop index AYUDA_FK;

drop index AYUDA_PK;

drop table AYUDA;

drop index POSEE_FK;

drop index CRITERIO_PK;

drop table CRITERIO;

drop index ESTUDIA2_FK;

drop index ESTUDIA_FK;

drop index ESTUDIA_PK;

drop table ESTUDIA;

drop index IMPARTE2_FK;

drop index IMPARTE_FK;

drop index IMPARTE_PK;

drop table IMPARTE;

drop index MODULO_PK;

drop table MODULO;

drop index OBTIENE2_FK;

drop index OBTIENE_FK;

drop index OBTIENE_PK;

drop table OBTIENE;

drop index PERTENECE_FK;

drop index TIENE_FK;

drop index PAUTA_PK;

drop table PAUTA;

drop index PERFIL_PK;

drop table PERFIL;

drop index COMPONE_FK;

drop index PREGUNTA_PK;

drop table PREGUNTA;

drop index TIPO_PK;

drop table TIPO;

drop index ROL_FK;

drop index USUARIO_PK;

drop table USUARIO;

/*==============================================================*/
/* Table: AYUDA                                                 */
/*==============================================================*/
create table AYUDA (
   CORREO_USUARIO       VARCHAR(256)         not null,
   ID_MODULO            INT4                 not null,
   constraint PK_AYUDA primary key (CORREO_USUARIO, ID_MODULO)
);

/*==============================================================*/
/* Index: AYUDA_PK                                              */
/*==============================================================*/
create unique index AYUDA_PK on AYUDA (
CORREO_USUARIO,
ID_MODULO
);

/*==============================================================*/
/* Index: AYUDA_FK                                              */
/*==============================================================*/
create  index AYUDA_FK on AYUDA (
CORREO_USUARIO
);

/*==============================================================*/
/* Index: AYUDA2_FK                                             */
/*==============================================================*/
create  index AYUDA2_FK on AYUDA (
ID_MODULO
);

/*==============================================================*/
/* Table: CRITERIO                                              */
/*==============================================================*/
create table CRITERIO (
   ID_CRITERIO          INT4                 not null,
   ID_PREGUNTA          INT4                 not null,
   NOMBRE_CRITERIO      VARCHAR(1024)        null,
   PUNTAJE              INT4                 null,
   constraint PK_CRITERIO primary key (ID_CRITERIO)
);

/*==============================================================*/
/* Index: CRITERIO_PK                                           */
/*==============================================================*/
create unique index CRITERIO_PK on CRITERIO (
ID_CRITERIO
);

/*==============================================================*/
/* Index: POSEE_FK                                              */
/*==============================================================*/
create  index POSEE_FK on CRITERIO (
ID_PREGUNTA
);

/*==============================================================*/
/* Table: ESTUDIA                                               */
/*==============================================================*/
create table ESTUDIA (
   ID_MODULO            INT4                 not null,
   CORREO_USUARIO       VARCHAR(256)         not null,
   constraint PK_ESTUDIA primary key (ID_MODULO, CORREO_USUARIO)
);

/*==============================================================*/
/* Index: ESTUDIA_PK                                            */
/*==============================================================*/
create unique index ESTUDIA_PK on ESTUDIA (
ID_MODULO,
CORREO_USUARIO
);

/*==============================================================*/
/* Index: ESTUDIA_FK                                            */
/*==============================================================*/
create  index ESTUDIA_FK on ESTUDIA (
ID_MODULO
);

/*==============================================================*/
/* Index: ESTUDIA2_FK                                           */
/*==============================================================*/
create  index ESTUDIA2_FK on ESTUDIA (
CORREO_USUARIO
);

/*==============================================================*/
/* Table: IMPARTE                                               */
/*==============================================================*/
create table IMPARTE (
   CORREO_USUARIO       VARCHAR(256)         not null,
   ID_MODULO            INT4                 not null,
   constraint PK_IMPARTE primary key (CORREO_USUARIO, ID_MODULO)
);

/*==============================================================*/
/* Index: IMPARTE_PK                                            */
/*==============================================================*/
create unique index IMPARTE_PK on IMPARTE (
CORREO_USUARIO,
ID_MODULO
);

/*==============================================================*/
/* Index: IMPARTE_FK                                            */
/*==============================================================*/
create  index IMPARTE_FK on IMPARTE (
CORREO_USUARIO
);

/*==============================================================*/
/* Index: IMPARTE2_FK                                           */
/*==============================================================*/
create  index IMPARTE2_FK on IMPARTE (
ID_MODULO
);

/*==============================================================*/
/* Table: MODULO                                                */
/*==============================================================*/
create table MODULO (
   NOMBRE_MODULO        VARCHAR(100)         null,
   ID_MODULO            INT4                 not null,
   constraint PK_MODULO primary key (ID_MODULO)
);

/*==============================================================*/
/* Index: MODULO_PK                                             */
/*==============================================================*/
create unique index MODULO_PK on MODULO (
ID_MODULO
);

/*==============================================================*/
/* Table: OBTIENE                                               */
/*==============================================================*/
create table OBTIENE (
   CORREO_USUARIO       VARCHAR(256)         not null,
   ID_CRITERIO          INT4                 not null,
   PORCENTAJE           FLOAT8               null,
   constraint PK_OBTIENE primary key (CORREO_USUARIO, ID_CRITERIO)
);

/*==============================================================*/
/* Index: OBTIENE_PK                                            */
/*==============================================================*/
create unique index OBTIENE_PK on OBTIENE (
CORREO_USUARIO,
ID_CRITERIO
);

/*==============================================================*/
/* Index: OBTIENE_FK                                            */
/*==============================================================*/
create  index OBTIENE_FK on OBTIENE (
CORREO_USUARIO
);

/*==============================================================*/
/* Index: OBTIENE2_FK                                           */
/*==============================================================*/
create  index OBTIENE2_FK on OBTIENE (
ID_CRITERIO
);

/*==============================================================*/
/* Table: PAUTA                                                 */
/*==============================================================*/
create table PAUTA (
   ID_PAUTA             INT4                 not null,
   ID_MODULO            INT4                 not null,
   ID_TIPO              INT4                 not null,
   NOMBRE_PAUTA         VARCHAR(200)         null,
   FECHA_CREA           DATE                 null,
   constraint PK_PAUTA primary key (ID_PAUTA)
);

/*==============================================================*/
/* Index: PAUTA_PK                                              */
/*==============================================================*/
create unique index PAUTA_PK on PAUTA (
ID_PAUTA
);

/*==============================================================*/
/* Index: TIENE_FK                                              */
/*==============================================================*/
create  index TIENE_FK on PAUTA (
ID_MODULO
);

/*==============================================================*/
/* Index: PERTENECE_FK                                          */
/*==============================================================*/
create  index PERTENECE_FK on PAUTA (
ID_TIPO
);

/*==============================================================*/
/* Table: PERFIL                                                */
/*==============================================================*/
create table PERFIL (
   ID_PERFIL            INT4                 not null,
   NOMBRE_PERFIL        VARCHAR(100)         null,
   constraint PK_PERFIL primary key (ID_PERFIL)
);

/*==============================================================*/
/* Index: PERFIL_PK                                             */
/*==============================================================*/
create unique index PERFIL_PK on PERFIL (
ID_PERFIL
);

/*==============================================================*/
/* Table: PREGUNTA                                              */
/*==============================================================*/
create table PREGUNTA (
   ID_PREGUNTA          INT4                 not null,
   ID_PAUTA             INT4                 not null,
   NOMBRE_PREGUNTA      VARCHAR(256)         null,
   NUMERO_PREGUNTA      INT4                 null,
   PUNTAJE              INT4                 null,
   constraint PK_PREGUNTA primary key (ID_PREGUNTA)
);

/*==============================================================*/
/* Index: PREGUNTA_PK                                           */
/*==============================================================*/
create unique index PREGUNTA_PK on PREGUNTA (
ID_PREGUNTA
);

/*==============================================================*/
/* Index: COMPONE_FK                                            */
/*==============================================================*/
create  index COMPONE_FK on PREGUNTA (
ID_PAUTA
);

/*==============================================================*/
/* Table: TIPO                                                  */
/*==============================================================*/
create table TIPO (
   ID_TIPO              INT4                 not null,
   NOMBRE_TIPO          VARCHAR(100)         null,
   constraint PK_TIPO primary key (ID_TIPO)
);

/*==============================================================*/
/* Index: TIPO_PK                                               */
/*==============================================================*/
create unique index TIPO_PK on TIPO (
ID_TIPO
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO (
   NOMBRE_USUARIO       VARCHAR(100)         null,
   CORREO_USUARIO       VARCHAR(256)         not null,
   ID_PERFIL            INT4                 not null,
   PASSWORD_USUARIO     VARCHAR(256)         null,
   constraint PK_USUARIO primary key (CORREO_USUARIO)
);

/*==============================================================*/
/* Index: USUARIO_PK                                            */
/*==============================================================*/
create unique index USUARIO_PK on USUARIO (
CORREO_USUARIO
);

/*==============================================================*/
/* Index: ROL_FK                                                */
/*==============================================================*/
create  index ROL_FK on USUARIO (
ID_PERFIL
);

alter table AYUDA
   add constraint FK_AYUDA_AYUDA_USUARIO foreign key (CORREO_USUARIO)
      references USUARIO (CORREO_USUARIO)
      on delete cascade on update cascade;

alter table AYUDA
   add constraint FK_AYUDA_AYUDA2_MODULO foreign key (ID_MODULO)
      references MODULO (ID_MODULO)
      on delete cascade on update cascade;

alter table CRITERIO
   add constraint FK_CRITERIO_POSEE_PREGUNTA foreign key (ID_PREGUNTA)
      references PREGUNTA (ID_PREGUNTA)
      on delete cascade on update cascade;

alter table ESTUDIA
   add constraint FK_ESTUDIA_ESTUDIA_MODULO foreign key (ID_MODULO)
      references MODULO (ID_MODULO)
      on delete cascade on update cascade;

alter table ESTUDIA
   add constraint FK_ESTUDIA_ESTUDIA2_USUARIO foreign key (CORREO_USUARIO)
      references USUARIO (CORREO_USUARIO)
      on delete cascade on update cascade;

alter table IMPARTE
   add constraint FK_IMPARTE_IMPARTE_USUARIO foreign key (CORREO_USUARIO)
      references USUARIO (CORREO_USUARIO)
      on delete cascade on update cascade;

alter table IMPARTE
   add constraint FK_IMPARTE_IMPARTE2_MODULO foreign key (ID_MODULO)
      references MODULO (ID_MODULO)
      on delete cascade on update cascade;

alter table OBTIENE
   add constraint FK_OBTIENE_OBTIENE_USUARIO foreign key (CORREO_USUARIO)
      references USUARIO (CORREO_USUARIO)
      on delete cascade on update cascade;

alter table OBTIENE
   add constraint FK_OBTIENE_OBTIENE2_CRITERIO foreign key (ID_CRITERIO)
      references CRITERIO (ID_CRITERIO)
      on delete cascade on update cascade;

alter table PAUTA
   add constraint FK_PAUTA_PERTENECE_TIPO foreign key (ID_TIPO)
      references TIPO (ID_TIPO)
      on delete restrict on update restrict;

alter table PAUTA
   add constraint FK_PAUTA_TIENE_MODULO foreign key (ID_MODULO)
      references MODULO (ID_MODULO)
      on delete restrict on update restrict;

alter table PREGUNTA
   add constraint FK_PREGUNTA_COMPONE_PAUTA foreign key (ID_PAUTA)
      references PAUTA (ID_PAUTA)
      on delete cascade on update cascade;

alter table USUARIO
   add constraint FK_USUARIO_ROL_PERFIL foreign key (ID_PERFIL)
      references PERFIL (ID_PERFIL)
      on delete restrict on update restrict;


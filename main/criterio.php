<?php 
	session_start();
	require_once("../lib/comun.php");
	require_once '../lib/conexion.php';
	$conexion = conectar();
	validarSesion();
	
 ?>

<!DOCTYPE html>
<html lang="es">
<head>
<title>HDEP</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="BHost template project">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap-4.1.2/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link href="plugins/video-js/video-js.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/about.css">
<link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
<link rel="stylesheet" type="text/css" href="alertifyjs/css/alertify.css">
<link rel="stylesheet" type="text/css" href="alertifyjs/css/themes/default.css">
<link rel="stylesheet" href="styles/icon.css">
<link rel="import" href="navbar.php">
<link rel="stylesheet" type="text/css" href="DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
<script src="js/jquery-3.2.1.min.js"></script>
</head>
<body>

<?php


$id_pregunta=$_GET['id_pre'];
$id_pauta=$_GET['id_pau'];
$id_modulo=$_GET['id_modul'];

	$verif=0;
	$corr=$_SESSION['correo'];

	$sql_ver="SELECT distinct correo_usuario from imparte where correo_usuario='$corr' and id_modulo='$id_modulo'";
					 	$querix=pg_query($conexion,$sql_ver);
					 	$contador=0;
					 	if(pg_num_rows($querix)>0){
							while(pg_num_rows($querix)>$contador){
								$datos= pg_fetch_result($querix,$contador,0);

								if($datos==$corr){
								$verif=1;	
								}
							$contador++;
						}
					}
	if ($verif==0) {
	header ("Location:index.php");  
	exit; 
	}



$consulta="SELECT sum(puntaje) as suma_total from pregunta as pre join posee as po on pre.id_pregunta=po.id_pregunta join criterio as cri on po.id_criterio=cri.id_criterio where pre.id_pregunta='$id_pregunta'";

$queri0=pg_query($conexion,$consulta);
$nr0=pg_num_rows($queri0);
$verificar0='';
	
if($nr0>=0){
	while($filas0=pg_fetch_array($queri0)){
	$verificar0 = $filas0["suma_total"];
	}
}
	$sqlu="UPDATE pregunta set puntaje_total='$verificar0' where id_pregunta='$id_pregunta'";
	echo $result=pg_query($conn,$sqlu);


$username=$_SESSION['username'];

$identificar="select id_perfil from usuario where nombre_usuario='$username'";
$queri=pg_query($conexion,$identificar);
$contador=1;
$nr=pg_num_rows($queri);
$verificar='';
	
if($nr>=0){
	while($filas=pg_fetch_array($queri)){
	$verificar = $filas["id_perfil"];
	}
}

$sqlx="SELECT distinct nombre_criterio, po.id_criterio from criterio as cri join posee as po on cri.id_criterio=po.id_criterio join pregunta as pre on po.id_pregunta=pre.id_pregunta join pauta as pa on pre.id_pauta=pa.id_pauta join modulo as mod on pa.id_modulo=mod.id_modulo where pa.id_modulo='$id_modulo'";

if($verificar==1 or $verificar==2){
$sql="SELECT distinct cri.id_criterio, cri.nombre_criterio, po.puntaje, po.id_pregunta FROM criterio as cri join posee as po on cri.id_criterio=po.id_criterio join pregunta as pre on po.id_pregunta=pre.id_pregunta join pauta as pa on pa.id_pauta=pre.id_pauta join modulo as mod on pa.id_modulo=mod.id_modulo join imparte as imp on imp.id_modulo=mod.id_modulo where po.id_pregunta='$id_pregunta' order by cri.id_criterio";
}
if($verificar==4){
$sql="SELECT distinct cri.id_criterio, cri.nombre_criterio, po.puntaje, po.id_pregunta FROM criterio as cri join posee as po on cri.id_criterio=po.id_criterio join pregunta as pre on po.id_pregunta=pre.id_pregunta join pauta as pa on pa.id_pauta=pre.id_pauta join modulo as mod on pa.id_modulo=mod.id_modulo join ayuda as ayu on ayu.id_modulo=mod.id_modulo where po.id_pregunta='$id_pregunta' order by cri.id_criterio";
}
if($verificar==3){
$sql="SELECT distinct cri.id_criterio, cri.nombre_criterio, po.puntaje, po.id_pregunta FROM criterio as cri join posee as po on cri.id_criterio=po.id_criterio join pregunta as pre on po.id_pregunta=pre.id_pregunta join pauta as pa on pa.id_pauta=pre.id_pauta join modulo as mod on pa.id_modulo=mod.id_modulo join estudia as est on est.id_modulo=mod.id_modulo where po.id_pregunta='$id_pregunta' order by cri.id_criterio";
}
?>

<div class="super_container">
	<div id="container-nav">
		</div>
		<!-- Home -->

		<div class="home">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="home_image"><img src="images/about_page.png" alt=""></div>
							<?php 
								$sql_crit="SELECT nombre_pregunta FROM pregunta WHERE id_pregunta='$id_pregunta'";
								$query_crit=pg_query($conexion,$sql_crit);
								$val_crit=pg_fetch_result($query_crit,0,0);
							 ?>
							<div class="home_title"><?php echo "Administrar Criterios de ".$val_crit; ?></div>		
						</div>
					</div>
				</div>
			</div>
			
			<!--<div class="domain_search_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="domain_search">
								<div class="domain_search_background"></div>
								<div class="domain_search_overlay"></div>
								<form action="#" class="domain_search_form" id="domain_search_form">
									<input type="text" class="domain_search_input" placeholder="Filtrar" required="required">
									<button class="domain_search_button">Buscar</button>
								</form>
							</div>
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</div> 


<div id="add_modal2" class="modal fade">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<form id="add_form2" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Crear planilla</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<input id="id_accion1" type="hidden" name="id_accion" value="1"/>					
						<div class="form-group">	
						<label>Nombre Criterio</label>
						<input type="text" class="form-control" name="nombre_criterio" id="nombre_criterio" required>

						</div>

						<div class="form-group">	
						<label>Puntaje</label>
						<input type="number" class="form-control" name="puntaje" id="puntaje" required>

						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Agregar" id="add_boton2">
					</div>
				</form>
			</div>
		</div>
	</div>

			<div class="pricing_content d-flex flex-column align-items-center justify-content-start">
		<div class="col-4 align-self-start">
			<button class="domain_search_button" data-toggle="modal" data-target="#add_modal2" id="<?php echo $id_criterio ?>" style="left: 500px;bottom: 0px;top: 64px;">Agregar Criterio</button>
		</div>
		</div>

	<!-- Modal Agregar -->

<div id="add_modal" class="modal fade">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<form id="add_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Crear planilla</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<input id="id_accion1" type="hidden" name="id_accion" value="1"/>					
						<div class="form-group">	

						<label>Coleccion de Criterios</label>
						<select id="slots" name="slots" class="form-control">
						<?php 

					 	$querix=pg_query($conexion,$sqlx);
					 	$contador=0;
					 	if(pg_num_rows($querix)>0){
							while(pg_num_rows($querix)>$contador){
								$datos= pg_fetch_result($querix,$contador,0);
								$datoss= pg_fetch_result($querix,$contador,1);

						?>
						
      					<option value="<?php echo $datoss;  ?>"><?php echo $datos;  ?></option>
      			



      					            <?php 
						$contador++;
						}
					}
				 ?>
      					</div>

      					<label>Coleccion de Criterios</label>
						<select id="drok" name="deok" class="">
      					</div>

						<div class="form-group">	
						<label>Puntaje</label>
						<input type="number" class="form-control" name="puntaje" id="puntaje" required>

						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Agregar" id="add_boton">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Fin modal add >

	<-- Modal delete -->	
	<div id="delete_modal" class="modal fade">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<form id="delete_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Borrar Criterio</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>¿Está seguro que desea borrar este registro?</p>
						<p class="text-warning"><small>Esta acción no se puede deshacer</small></p>
					</div>
					<div class="modal-footer">
						<input id="input_delete" type="hidden" name="id_criterio" value=""/>
						<input id="id_accion3" type="hidden" name="id_accion" value="3"/>
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-danger" id="btn_delete" value="Borrar">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Fin modal delete > 

	<-- Modal Update-->
	<div id="edit_modal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="edit_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Editar Criterio</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<input id="id_criterioU" type="hidden" name="id_criterioU" value=""/>
							
							<label>Nombre Criterio</label>
							<input type="text" class="form-control" name="nombre_criterioU" id="nombre_criterioU" required>
						</div>
						<div class="form-group">
							<label>Puntaje</label>
							<input id="id_accion2" type="hidden" name="id_accion" value="2"/>
							<input type="number" class="form-control" name="puntajeU" id="puntajeU" required>
							
						</div>						
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-info" value="Actualizar" id="btn_update">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--fin modal update-->
		<br><br><br>

		<div class="pricing_content d-flex flex-column align-items-center justify-content-start">
		<div class="col-4 align-self-start">
			<button class="domain_search_button" data-toggle="modal" data-target="#add_modal" id="<?php echo $id_criterio ?>">Repetir Criterio</button>
		</div>
		</div>
		
		<!-- Team -->
		<div class="container" id="container_tabla">
			<div class="section_title_container text-center">
				<div class="row team_row">
					<div class="team_text"></div>
	                <div class="table-responsive">
	                <table class="table" id="id_tabla" >
	                    <thead>
	                        <tr>
	                            <th class="text-center">#</th>
	                            <th>Nombre Criterio</th>
	                            <th>Puntaje</th>
	                            <th>Acciones</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php
	                    	$contador=1;
	                    	$result=pg_query($conexion,$sql);
	                    	
	                    	while ($datos=pg_fetch_row($result)) {
	                    		$update= $datos[0]."||".$datos[1]."||".$datos[2];
	                    	?>
	                    		<tr>
		                            <td class="text-center"><?php echo $contador;?></td>
		                            <td><?php echo $datos[1]; ?></td>
		                            <td><?php echo $datos[2]; ?></td>
		                            <td>
	                            		<a href="#delete_modal" class="text-danger" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Borrar" onclick="id_delete('<?php echo $datos[0]; ?>')">&#xE872;</i></a>
	                            	</td>
	                        	</tr>
	                        	
	
	                    	<?php	
	                    	$contador++;
	                    	}
	                    	?>
	                    </tbody>
	                </table>
	                </div>
                </div>
            </div>
        </div>
			<!-- Fin modal delete --> 
	</div>

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="styles/bootstrap-4.1.2/popper.js"></script>
	<script src="styles/bootstrap-4.1.2/bootstrap.min.js"></script>
	<script src="plugins/greensock/TweenMax.min.js"></script>
	<script src="plugins/greensock/TimelineMax.min.js"></script>
	<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="plugins/greensock/animation.gsap.min.js"></script>
	<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="plugins/easing/easing.js"></script>
	<script src="plugins/progressbar/progressbar.min.js"></script>
	<script src="plugins/parallax-js-master/parallax.min.js"></script>
	<script src="plugins/video-js/video.min.js"></script>
	<script src="plugins/video-js/Youtube.min.js"></script>
	<script src="js/about.js"></script>
	<script src="js/importar_html.js"></script>
	<script src="alertifyjs/alertify.js"></script>
	<script src="DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
</body>
</html>
<script type="text/javascript">
//ajax para agregar a la base de datos
$(document).ready(function(){
      $('#add_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud_criterio.php?id_pre=<?php echo $id_pregunta?>&&id_modul=<?php echo $id_modulo;?>",
    method:"POST",
    data:$('#add_form').serialize(),
    beforeSend:function(){
     $('#add_boton').val("Agregando");
    },
    success:function(data){
         $('#add_form')[0].reset();
         $('#add_modal').modal('hide');
         $('#add_boton').val("Agregar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Ingresando");
     }
    });
});
});
</script>




<script type="text/javascript">
//ajax para agregar a la base de datos
$(document).ready(function(){
      $('#add_form2').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud_criterio.php?id_pre=<?php echo $id_pregunta?>&&id_modul=<?php echo $id_modulo;?>",
    method:"POST",
    data:$('#add_form2').serialize(),
    beforeSend:function(){
     $('#add_boton2').val("Agregando");
    },
    success:function(data){
         $('#add_form2')[0].reset();
         $('#add_modal2').modal('hide');
         $('#add_boton2').val("Agregar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
     	 alertify.success("Ingreso con exito");
     }

    });
});
});
</script>




<script type="text/javascript">
//script para enviar el id de correo al delete_modal
	function id_delete(id){
		$('#input_delete').val(id);
	}

</script>
<script type="text/javascript">
	
	function modulo_input(id){
		$('#modulo_ident').val(id);
	}

</script>

<script type="text/javascript">
//ajax para borrar datos de la base de datos
$(document).ready(function(){
      $('#delete_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud_criterio.php?id_pre=<?php echo $id_pregunta;?>",
    method:"POST",
    data:$('#delete_form').serialize(),
    beforeSend:function(){
     $('#btn_delete').val("Borrando");
    },
    success:function(data){
         $('#delete_form')[0].reset();
         $('#delete_modal').modal('hide');
         $('#btn_delete').val("Borrar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Eliminación con exito");
     }

    });
});
});
</script>

<script type="text/javascript">
	//script para enviar los datos al modal update
	function update_data_modal(data){
		d= data.split('||');

		$('#id_criterioU').val(d[0]);
		$('#nombre_criterioU').val(d[1]);
		$('#puntajeU').val(d[2]);

	}
</script>

<script type="text/javascript">
//ajax paa actualizar datos en la base de datos
$(document).ready(function(){
      $('#edit_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud_criterio.php?id_pre=<?php echo $id_pregunta;?>&&id_modul=<?php echo $id_modulo;?>",
    method:"POST",
    data:$('#edit_form').serialize(),
    beforeSend:function(){
     $('#btn_update').val("Actualizando");
    },
    success:function(data){
         $('#edit_form')[0].reset();
         $('#edit_modal').modal('hide');
         $('#btn_update').val("Actualizar");
         $('#btn_delete').val("Borrar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Actualización con exito");
     }

    });
});
});
</script>
<script type="text/javascript">
	//dataTable
	$(document).ready(function() {

    $('#id_tabla').DataTable( {
    	//responsive: true,
    	//"autoWidth": true,
    	"language": {
                "url": "DataTables/Spanish.json"
        }
	} );
} );

</script>
<script>
	$(function(){
  		$("#container-nav").load("navbar.php");
	});
</script>
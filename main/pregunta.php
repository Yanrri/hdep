<?php 
	session_start();
	require_once("../lib/comun.php");
	require_once '../lib/conexion.php';
	$conexion = conectar();
	validarSesion();
	
 ?>

<!DOCTYPE html>
<html lang="es">
<head>
<title>HDEP</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="BHost template project">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap-4.1.2/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link href="plugins/video-js/video-js.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/about.css">
<link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
<link rel="stylesheet" type="text/css" href="alertifyjs/css/alertify.css">
<link rel="stylesheet" type="text/css" href="alertifyjs/css/themes/default.css">
<link rel="stylesheet" href="styles/icon.css">
<link rel="import" href="navbar.php">
<link rel="stylesheet" type="text/css" href="DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
<script src="js/jquery-3.2.1.min.js"></script>
</head>
<body>

<?php


$id_modu=$_GET['id_modul'];
$id_pauta=$_GET['id_pau'];

	
	$verif=0;
	$corr=$_SESSION['correo'];

	$sql_ver="SELECT distinct correo_usuario from imparte where correo_usuario='$corr' and id_modulo='$id_modu'";
					 	$querix=pg_query($conexion,$sql_ver);
					 	$contador=0;
					 	if(pg_num_rows($querix)>0){
							while(pg_num_rows($querix)>$contador){
								$datos= pg_fetch_result($querix,$contador,0);

								if($datos==$corr){
								$verif=1;	
								}
							$contador++;
						}
					}
	if ($verif==0) {
	header ("Location:index.php");  
	exit; 
	}



$sqlo = "SELECT COUNT(*) nombre_pregunta FROM pregunta WHERE id_pauta='$id_pauta'";
$result2 = pg_query($sqlo);
$fila2 = pg_fetch_assoc($result2);
$numero_pregunta=$fila2['nombre_pregunta']+1;


$username=$_SESSION['username'];

$identificar="select id_perfil from usuario where nombre_usuario='$username'";
$queri=pg_query($conexion,$identificar);
$contador=1;
$nr=pg_num_rows($queri);
$verificar='';
	
if($nr>=0){
	while($filas=pg_fetch_array($queri)){
	$verificar = $filas["id_perfil"];
	}
}



if($verificar==1 or $verificar==2){
$sql="SELECT distinct nombre_pregunta, numero_pregunta , id_pregunta , puntaje_total from pregunta as pre join pauta as pa on pa.id_pauta=pre.id_pauta join modulo as mod on mod.id_modulo=pa.id_modulo join imparte as imp on imp.id_modulo=mod.id_modulo where pa.id_modulo='$id_modu' and pre.id_pauta='$id_pauta' order by numero_pregunta";
}
if($verificar==4){
$sql="select nombre_pregunta, numero_pregunta, id_pregunta , puntaje_total from pregunta as pre join pauta as pa on pa.id_pauta=pre.id_pauta join modulo as mod on mod.id_modulo=pa.id_modulo join ayuda as ayu on ayu.id_modulo=mod.id_modulo where pa.id_modulo='$id_modu' and pre.id_pauta='$id_pauta' order by numero_pregunta";
}
if($verificar==3){
$sql="select nombre_pregunta, numero_pregunta, id_pregunta , puntaje_total from pregunta as pre join pauta as pa on pa.id_pauta=pre.id_pauta join modulo as mod on mod.id_modulo=pa.id_modulo join estudia as es on es.id_modulo=mod.id_modulo where pa.id_modulo='$id_modu' and pre.id_pauta='$id_pauta' order by numero_pregunta";
}
?>

<div class="super_container">
	<div id="container-nav">
		</div>
		<!-- Home -->

		<div class="home">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="home_image"><img src="images/about_page.png" alt=""></div>
							<?php 
								$sql_preg="SELECT nombre_pauta FROM pauta WHERE id_modulo='$id_modu' AND id_pauta='$id_pauta'";
								$query_preg=pg_query($conexion,$sql_preg);
								$val_preg=pg_fetch_result($query_preg,0,0);
							 ?>
							<div class="home_title"><?php echo "Administrar Preguntas de ".$val_preg ?></div>		
						</div>
					</div>
				</div>
			</div>
			
			<!--<div class="domain_search_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="domain_search">
								<div class="domain_search_background"></div>
								<div class="domain_search_overlay"></div>
								<form action="#" class="domain_search_form" id="domain_search_form">
									<input type="text" class="domain_search_input" placeholder="Filtrar" required="required">
									<button class="domain_search_button">Buscar</button>
								</form>
							</div>
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</div> 


	<!-- Modal Agregar -->

	<div id="add_modal" class="modal fade">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<form id="add_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Crear planilla</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<input id="id_accion1" type="hidden" name="id_accion" value="1"/>					
						<div class="form-group">	

						<label>Pregunta</label>
						<input type="text" class="form-control" name="nombre_pregunta" id="nombre_pregunta" required>
						<label>Numero Pregunta</label>
						<input type="number" class="form-control" name="numero_pregunta" id="numero_pregunta" required>

						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Agregar" id="add_boton">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Fin modal add >

	<-- Modal delete -->	
	<div id="delete_modal" class="modal fade">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<form id="delete_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Borrar Usuario</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>¿Está seguro que desea borrar este registro?</p>
						<p class="text-warning"><small>Esta acción no se puede deshacer</small></p>
					</div>
					<div class="modal-footer">
						<input id="input_delete" type="hidden" name="id_pregunta" value=""/>
						<input id="id_accion3" type="hidden" name="id_accion" value="3"/>
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-danger" id="btn_delete" value="Borrar">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Fin modal delete > 

	<-- Modal Update-->
	<div id="edit_modal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="edit_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Editar Pregunta</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Nombre Pregunta</label>
							<input type="text" class="form-control" name="nombre_preguntaU" id="nombre_preguntaU" required>
							<input id="id_accion2" type="hidden" name="id_accion" value="2"/>
							<input id="id_preguntaU" type="hidden" name="id_preguntaU" value=""/>


						</div>			
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-info" value="Actualizar" id="btn_update">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--fin modal update-->
		<br><br><br>

		<div class="pricing_content d-flex flex-column align-items-center justify-content-start">
		<div class="col-4 align-self-start">
			<button class="domain_search_button" data-toggle="modal" data-target="#add_modal" id="<?php echo $id_pregunta ?>">+ Agregar</button>
		</div>
		</div>
		
		<!-- Team -->
		<div class="container" id="container_tabla">
			<div class="section_title_container text-center">
				<div class="row team_row">
					<div class="team_text"></div>
	                <div class="table-responsive">
	                <table class="table" id="id_tabla" >
	                    <thead>
	                        <tr>
	                            <th class="text-center">#</th>
	                            <th>Pregunta</th>
	                            <th>Puntaje Pregunta</th>
	                            <th>Acciones</th>
	                            <th>Ver</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php
	                    	$contador=1;
	                    	$result=pg_query($conexion,$sql);
	                    	
	                    	while ($datos=pg_fetch_row($result)) {
	                    		$update= $datos[0]."||".$datos[1]."||".$datos[2];
	                    		$id_m=$datos[2];
	                    	?>
	                    		<tr>
		                            <td class="text-center"><?php echo $contador;?></td>
		                            <td><?php echo $datos[0]; ?></td>
		                            <td><?php echo $datos[3]; ?></td>
		                            <td>
		                            	<a href="#edit_modal" class="text-warning" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Editar" onclick="update_data_modal('<?php echo $update; ?>')">&#xE254;</i></a>
	                            		<a href="#delete_modal" class="text-danger" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Borrar" onclick="id_delete('<?php echo $datos[2]; ?>')">&#xE872;</i></a>
	                            	</td>

		                            <td class="center">
		                            	<div class="buttton btn btn-md btn-info"><a href="criterio.php?id_pre=<?php echo $datos[2];?>&&id_modul=<?php echo $id_modu;?>"> Criterios</a></div>
		                     
	                            	</td>
	                        	</tr>
	                        	
	
	                    	<?php	
	                    	$contador++;
	                    	}
	                    	?>
	                    </tbody>
	                </table>
	                </div>
                </div>
            </div>
        </div>
     	<!-- Modal -->
		<div class="modal fade" id="participantes_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-header text-center">
		        <h4 class="modal-title w-100" id="exampleModalLabel">Criterios</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body" id="detalles_modulo">

	        	<!-- Tabla modal -->
	        	<!--<div class="container">
	        	</div>-->
	        	<div id="id_tabla_modal"></div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
		        <button type="button" class="btn btn-primary">Guardar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- modal agregar participantes-->
		<div class="modal" tabindex="-1" role="dialog" id="add_participantes">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">Agregar Criterio</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		    <div class="modal-body">
		        <form id="form_participantes" method="post">
		        	<input id="id_accion2" type="hidden" name="id_accionP" value="1"/>
  					<div class="form-group">
  						<label>Nombre Criterio</label>
							<input type="text" class="form-control" name="nombre_criterio" id="nombre_criterio" required>
						<label>Puntaje</label>
							<input type="number" class="form-control" name="puntaje" id="puntaje" required>
					</div>
		        
		    </div>
		    <div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
					<input type="submit" class="btn btn-success" value="Ingresar" id="btn_add2">
				</form>
			</div>
		    </div>
		  </div>
		</div>
        <!-- fin modal -->
        <!-- Modal delete partocipantes -->	
			<div id="delete_modal2" class="modal fade">
				<div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">
						<form id="delete_form2" method="post">
							<div class="modal-header">						
								<h4 class="modal-title">Borrar Usuario</h4>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body">					
								<p>¿Está seguro que desea borrar este registro?</p>
								<p class="text-warning"><small>Esta acción no se puede deshacer</small></p>
							</div>
							<div class="modal-footer">
								<input id="input_delete_id" type="hidden" name="id_pregunta" value=""/>
								<input id="input_delete_c" type="hidden" name="id_criterio" value=""/>
								<input id="id_accionP" type="hidden" name="id_accionP" value="3"/>
								<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
								<input type="submit" class="btn btn-danger" id="btn_delete2" value="Borrar">
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- Fin modal delete --> 
	</div>

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="styles/bootstrap-4.1.2/popper.js"></script>
	<script src="styles/bootstrap-4.1.2/bootstrap.min.js"></script>
	<script src="plugins/greensock/TweenMax.min.js"></script>
	<script src="plugins/greensock/TimelineMax.min.js"></script>
	<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="plugins/greensock/animation.gsap.min.js"></script>
	<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="plugins/easing/easing.js"></script>
	<script src="plugins/progressbar/progressbar.min.js"></script>
	<script src="plugins/parallax-js-master/parallax.min.js"></script>
	<script src="plugins/video-js/video.min.js"></script>
	<script src="plugins/video-js/Youtube.min.js"></script>
	<script src="js/about.js"></script>
	<script src="js/importar_html.js"></script>
	<script src="alertifyjs/alertify.js"></script>
	<script src="DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
</body>
</html>
<script type="text/javascript">
//ajax para agregar a la base de datos
$(document).ready(function(){
      $('#add_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud_pregunta.php?contar=<?php echo $contador;?>&&id_pre=<?php echo $id_pregunta;?>&&id_pau=<?php echo $id_pauta;?>",
    method:"POST",
    data:$('#add_form').serialize(),
    beforeSend:function(){
     $('#add_boton').val("Agregando");
    },
    success:function(data){
         $('#add_form')[0].reset();
         $('#add_modal').modal('hide');
         $('#add_boton').val("Agregar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Ingreso con exito");
     }

    });
});
});
</script>
<script type="text/javascript">
//script para enviar el id de correo al delete_modal
	function id_delete(id){
		$('#input_delete').val(id);
	}

</script>
<script type="text/javascript">
	
	function modulo_input(id){
		$('#modulo_ident').val(id);
	}

</script>

<script type="text/javascript">
//ajax para borrar datos de la base de datos
$(document).ready(function(){
      $('#delete_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud_pregunta.php",
    method:"POST",
    data:$('#delete_form').serialize(),
    beforeSend:function(){
     $('#btn_delete').val("Borrando");
    },
    success:function(data){
         $('#delete_form')[0].reset();
         $('#delete_modal').modal('hide');
         $('#btn_delete').val("Borrar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Eliminación con exito");
     }

    });
});
});
</script>

<script type="text/javascript">
	//script para enviar los datos al modal update
	function update_data_modal(data){
		d= data.split('||');

		$('#nombre_preguntaU').val(d[0]);
		$('#id_preguntaU').val(d[2]);

	}
</script>

<script type="text/javascript">
//ajax paa actualizar datos en la base de datos
$(document).ready(function(){
      $('#edit_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud_pregunta.php",
    method:"POST",
    data:$('#edit_form').serialize(),
    beforeSend:function(){
     $('#btn_update').val("Actualizando");
    },
    success:function(data){
         $('#edit_form')[0].reset();
         $('#edit_modal').modal('hide');
         $('#btn_update').val("Actualizar");
         $('#btn_delete').val("Borrar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Actualización con exito");
     }

    });
});
});
</script>
<script type="text/javascript">
	//dataTable
	$(document).ready(function() {

    $('#id_tabla').DataTable( {
    	//responsive: true,
    	//"autoWidth": true,
    	"language": {
                "url": "DataTables/Spanish.json"
        }
	} );
} );

</script>
<!--<script type="text/javascript">
	//dataTable
	$(document).ready(function() {

    $('#id_tabla_m').DataTable( {
    	//responsive: true,
    	//"autoWidth": true,
    	"language": {
                "url": "DataTables/Spanish.json"
        }
	} );
} );

</script>-->

<!--<script type="text/javascript">
	//script para recargar el contenido de la tabla
      $(document).ready(function(){
          $('#id_tabla_modal').load('tabla_modal.php');
      });
</script>-->

<script type="text/javascript">
	//ajax para enviar el id y crear la tabla dentro del modal
	$(document).ready(function(){
		$('.view_data').click(function(){
			var pregunta_id= $(this).attr("id");
			$.ajax({
				url:"tabla2.php",
				method:"post",
				async:false,
				data:{pregunta_id:pregunta_id},
				success:function(data){
					$('#detalles_modulo').html(data);
					$('#id_moduloU').val(pregunta_id);
					$('#participantes_modal').modal("show");
					/*$('#id_tabla_m').DataTable( {
				    	//responsive: true,
				    	//"autoWidth": true,
				    	"language": {
				                "url": "DataTables/Spanish.json"
				        }
					} );*/
				}
			});
		});
	});
</script>

<script type="text/javascript">
//ajax para agregar a la base de datos
$(document).ready(function(){
      $('#form_participantes').on("submit", function(event){
      	var pregunta_id= $(this).attr("int");
  event.preventDefault();
     $.ajax({
    url:"crudParticipantes2.php",
    method:"POST",
    data:{pregunta_id:pregunta_id},
    data:$('#form_participantes').serialize(),
    beforeSend:function(){
     $('#btn_add2').val("Agregando");
    },
    success:function(data){
         $('#add_form')[0].reset();
         $('#add_participantes').modal('hide');
         $('#btn_add2').val("Agregar");
         //$('#id_tabla_m').html(data);
         //$('#id_tabla_m').load(location.href+" #id_tabla_m>*","");
         $('#participantes_modal').modal('hide');
         alertify.success("Ingreso con exito");
     }

    });
});
});
</script>

<script type="text/javascript">
	function id_delete2(data){
		d=data.split('||');
		$('#input_delete_id').val(d[1]);
		$('#input_delete_c').val(d[0]);
	}
</script>

<script type="text/javascript">
//ajax para borrar participantes de la base de datos
$(document).ready(function(){
      $('#delete_form2').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crudParticipantes2.php",
    method:"POST",
    data:$('#delete_form2').serialize(),
    beforeSend:function(){
     $('#btn_delete2').val("Borrando");
    },
    success:function(data){
         $('#delete_form2')[0].reset();
         $('#delete_modal2').modal('hide');
         $('#btn_delete2').val("Borrar");
         $('#participantes_modal').modal('hide');
         //$('#id_tabla').html(data);
         //$('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Eliminación con exito");
     }

    });
});
});
</script>
<script>
	$(function(){
  		$("#container-nav").load("navbar.php");
	});
</script>
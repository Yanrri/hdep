<?php 
	session_start();
	require_once("../lib/comun.php");
	require_once '../lib/conexion.php';
	
	$conexion = conectar();
	validarSesion();
	
 ?>

<!DOCTYPE html>
<html lang="es">
<head>
<title>HDEP</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="BHost template project">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap-4.1.2/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link href="plugins/video-js/video-js.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/about.css">
<link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
<link rel="stylesheet" type="text/css" href="alertifyjs/css/alertify.css">
<link rel="stylesheet" type="text/css" href="alertifyjs/css/themes/default.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="import" href="navbar.php">
<link rel="stylesheet" type="text/css" href="DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
</head>
<body>
	
<?php
$fecha_actual=date("Y-m-d");
$username=$_SESSION['username'];
$id=$_GET['id'];



$identificar2="select id_pauta from pauta where id_pauta=(select max(id_pauta) from pauta)";
$queri2=pg_query($conexion,$identificar2);
$nr2=pg_num_rows($queri2);
$verificar2='';

if($nr2>=0){
	while($filas2=pg_fetch_array($queri2)){
		$verificar2 = $filas2["id_pauta"];
	}
}

$nuevo_id=$verificar2+1;


$identificar="select id_perfil from usuario where nombre_usuario='$username'";
$queri=pg_query($conexion,$identificar);
$contador=0;
$nr=pg_num_rows($queri);
$verificar='';
	
if($nr>=0){
	while($filas=pg_fetch_array($queri)){
	$verificar = $filas["id_perfil"];
	}
}



$sql="SELECT id_pauta, nombre_pauta, nombre_tipo, fecha_crea, tip.id_tipo from pauta as pa join tipo as tip on pa.id_tipo=tip.id_tipo  where id_modulo='$id' order by id_pauta";
$sql_title="SELECT nombre_modulo FROM modulo WHERE id_modulo='$id'";
?>

<div class="super_container">
	<div id="container-nav">
		</div>
		<!-- Home -->

		<div class="home">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="home_image"><img src="images/about_page.png" alt=""></div>
							<?php 
								$home=pg_query($conexion,$sql_title);
								$value=pg_fetch_result($home,0,0);	
							 ?>
							<div class="home_title"><?php echo "PAUTAS DE ".$value; ?> </div>		
						</div>
					</div>
				</div>
			</div>
			
			<!--<div class="domain_search_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="domain_search">
								<div class="domain_search_background"></div>
								<div class="domain_search_overlay"></div>
								<form action="#" class="domain_search_form" id="domain_search_form">
									<input type="text" class="domain_search_input" placeholder="Filtrar" required="required">
									<button class="domain_search_button">Buscar</button>
								</form>
							</div>
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</div> 
	
	<!-- Modal delete -->	
	<div id="delete_modal" class="modal fade">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<form id="delete_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Eliminar Pauta</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>¿Está seguro que desea borrar este registro?</p>
						<p class="text-warning"><small>Esta acción no se puede deshacer</small></p>
					</div>
					<div class="modal-footer">
						<input id="input_delete" type="hidden" name="id_pauta" value=""/>
						<input id="id_accion3" type="hidden" name="id_accion" value="3"/>
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-danger" id="btn_delete" value="Borrar">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Fin modal delete > 

	<-- Modal Update-->
	<div id="edit_modal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="edit_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Editar Pauta</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<input id="id_pautaU" type="hidden" name="id_pautaU" value=""/>
							<label>Nombre de Pauta</label>
							<input type="text" class="form-control" name="nombre_pautaU" id="nombre_pautaU" required>
						</div>
						<div class="form-group">
							<label>Tipo de Evaluacion</label><br>
							<select name="tipoU" class="browser-default custom-select" id="tipoU">
							<?php 
								$sql_tipo="SELECT id_tipo,nombre_tipo FROM tipo ORDER BY id_tipo";
								$result_tipo=pg_query($conexion,$sql_tipo);
								while($row_t=pg_fetch_assoc($result_tipo)){
									echo "<option value='$row_t[id_tipo]'>$row_t[nombre_tipo]</option>";
								}
							 ?>
							 </select>
							<!--<input id="id_accion2" type="hidden" name="id_accion" value="2"/>
							<input type="radio" name="tipoU" id="tipo1U" value="1">Evaluacion escrita<br>
							<input type="radio" name="tipoU" id="tipo2U" value="2">Evaluacion oral<br>
							<input type="radio" name="tipoU" id="tipo3U" value="3">Laboratorio<br>
							<input type="radio" name="tipoU" id="tipo4U" value="4">Informe<br>
							<input type="radio" name="tipoU" id="tipo5U" value="5">Tarea<br>-->
						</div>				
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-info" value="Actualizar" id="btn_update">
					</div>
				</form>
			</div>
		</div>
	</div>
	<br><br><br>

	<?php
	$verif=0;
	$corr=$_SESSION['correo'];

	$sql_ver="SELECT distinct correo_usuario from imparte where correo_usuario='$corr' and id_modulo='$id'";
					 	$querix=pg_query($conexion,$sql_ver);
					 	$contador=0;
					 	if(pg_num_rows($querix)>0){
							while(pg_num_rows($querix)>$contador){
								$datos= pg_fetch_result($querix,$contador,0);

								if($datos==$corr){
								$verif=1;	
								}
							$contador++;
						}
					}
	if ($verif==1) {
	 ?>

	
	<!-- Team -->
		<div class="container" id="container_tabla">
			<div class="section_title_container text-center">
				<div class="row team_row" style="margin-top: 0px;">
					<div class="team_text"></div>
	                <div class="table-responsive">
	                <table class="table" id="id_tabla" >
	                    <thead>
	                        <tr>
	                            <th class="text-center">#</th>
	                            <th>Nombre Pauta</th>
	                            <th>Tipo Evaluacion</th>
	                            <th>Fecha Creacion</th>
	                            <th>Acciones</th>
	                            <th>Ver</th>
	                            <th>Planilla</th>
								<th>Exportar</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php
	                    	$result=pg_query($conexion,$sql);
	                    	$contador=1;
	                    	while ($datos=pg_fetch_row($result)) {
	                    		$update= $datos[0]."||".$datos[1]."||".$datos[4];
	                    	?>
	                    		<tr>
		                            <td class="text-center"><?php echo $contador; ?></td>
		                            <td><?php echo $datos[1]; ?></td>
		                            <td><?php echo $datos[2]; ?></td>
		                            <td><?php echo $datos[3]; ?></td>
		                            
		                          
		                            <td>
		                            	<a href="#edit_modal" class="text-warning" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Editar" onclick="update_data_modal('<?php echo $update; ?>')">&#xE254;</i></a>
	                            		<a href="#delete_modal" class="text-danger" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Borrar" onclick="id_delete('<?php echo $datos[0]; ?>')">&#xE872;</i></a>
	                            	</td>

		                            <td class="center">
		                            	<div class="buttton btn btn-md btn-warning"><a style="color: #C7A200;"href="pregunta.php?id_pau=<?php echo $datos[0];?>&&id_modul=<?php echo $id;?>"> Preguntas</a></div>
		                     
	                            	</td>

	                            			                            <td class="center">
		                            	<div class="buttton btn btn-md btn-danger"><a style="color: #7D0512;" href="planillaEdit.php?id_pau=<?php echo $datos[0];?>&&id_modul=<?php echo $id;?>"> Visualizar</a></div>
		                     
	                            	</td>


	                            			                            <td class="center">
		                            	<div class="buttton btn btn-md btn-success"><a style="color: #008000;"  href="planilla.php?id_pau=<?php echo $datos[0];?>&&id_modul=<?php echo $id;?>"> Excel	</a></div>
		                     
	                            	</td>




	                        	</tr>
	                    	<?php
	                    		$contador++;	
	                    	}
	                    	?>

	                    </tbody>
	                </table>
	                </div>
                </div>
            </div>
        </div>

	 <?php
	}
		if ($verif==0) {
	 ?>

		<div class="container" id="container_tabla">
			<div class="section_title_container text-center">
				<div class="row team_row" style="margin-top: 0px;">
					<div class="team_text"></div>
	                <div class="table-responsive">
	                <table class="table" id="id_tabla" >
	                    <thead>
	                        <tr>
	                            <th class="text-center">#</th>
	                            <th>Nombre Pauta</th>
	                            <th>Tipo Evaluacion</th>
	                            <th>Fecha Creacion</th>
	                            <th>Planilla</th>
								<th>Exportar</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php
	                    	$result=pg_query($conexion,$sql);
	                    	$contador=1;
	                    	while ($datos=pg_fetch_row($result)) {
	                    		$update= $datos[0]."||".$datos[1]."||".$datos[4];
	                    	?>
	                    		<tr>
		                            <td class="text-center"><?php echo $contador; ?></td>
		                            <td><?php echo $datos[1]; ?></td>
		                            <td><?php echo $datos[2]; ?></td>
		                            <td><?php echo $datos[3]; ?></td>
		                            

	                            			                            <td class="center">
		                            	<div class="buttton btn btn-md btn-danger"><a style="color: #7D0512;" href="planillaEdit.php?id_pau=<?php echo $datos[0];?>&&id_modul=<?php echo $id;?>"> Visualizar</a></div>
		                     
	                            	</td>


	                            			                            <td class="center">
		                            	<div class="buttton btn btn-md btn-success"><a style="color: #008000;"  href="planilla.php?id_pau=<?php echo $datos[0];?>&&id_modul=<?php echo $id;?>"> Excel	</a></div>
		                     
	                            	</td>




	                        	</tr>
	                    	<?php
	                    		$contador++;	
	                    	}
	                    	?>

	                    </tbody>
	                </table>
	                </div>
                </div>
            </div>
        </div>

        	 <?php
	}
	 ?>

	</div>


	<div id="add_modal" class="modal fade">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<form id="add_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Crear planilla</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<input id="id_accion1" type="hidden" name="id_accion" value="1"/>					
						<div class="form-group">
					<label>Nombre Pauta</label>
							<input type="text" class="form-control" name="nombre_pauta" id="nombre_pauta" required>


							<div class="form-group">
							<label>Tipo Pauta</label><br>
							<select name="perfil" class="browser-default custom-select">
							<?php 
								$sql_tipo="SELECT id_tipo,nombre_tipo FROM tipo ORDER BY id_tipo";
								$result_tipo=pg_query($conexion,$sql_tipo);
								while($row_t=pg_fetch_assoc($result_tipo)){
									echo "<option value='$row_t[id_tipo]'>$row_t[nombre_tipo]</option>";
								}
							 ?>
							 </select>
							<!--<input type="radio" name="perfil" id="perfil1" value="1">Evaluacion escrita<br>
							<input type="radio" name="perfil" id="perfil2" value="2">Evaluacion oral<br>
							<input type="radio" name="perfil" id="perfil3" value="3">Laboratorio<br>
							<input type="radio" name="perfil" id="perfil4" value="4">Informe<br>
							<input type="radio" name="perfil" id="perfil5" value="5">Tarea<br>-->
						</div>
						
						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Agregar" id="add_boton">
					</div>
				</form>
			</div>
		</div>
	</div>

										<?php
										if ($verif==1) {
											?>
										<div class="pricing_content d-flex flex-column align-items-center justify-content-start">
									<div class="button pricing_button trans_200" data-toggle="modal" data-target="#add_modal" style="margin-bottom: 20px;"id="<?php echo $id ?>"><a> Crear Pauta +</a></div>
									</div>
									<?php
								}
									?>

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="styles/bootstrap-4.1.2/popper.js"></script>
	<script src="styles/bootstrap-4.1.2/bootstrap.min.js"></script>
	<script src="plugins/greensock/TweenMax.min.js"></script>
	<script src="plugins/greensock/TimelineMax.min.js"></script>
	<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="plugins/greensock/animation.gsap.min.js"></script>
	<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="plugins/easing/easing.js"></script>
	<script src="plugins/progressbar/progressbar.min.js"></script>
	<script src="plugins/parallax-js-master/parallax.min.js"></script>
	<script src="plugins/video-js/video.min.js"></script>
	<script src="plugins/video-js/Youtube.min.js"></script>
	<script src="js/about.js"></script>
	<script src="js/importar_html.js"></script>
	<script src="alertifyjs/alertify.js"></script>
	<script src="DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
</body>
</html>



<script type="text/javascript">
//ajax para agregar a la base de datos
$(document).ready(function(){
      $('#add_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud_pauta.php?id_mod=<?php echo $id;?>&&nuevo_id2=<?php echo $nuevo_id;?>&&fecha=<?php echo $fecha_actual;?>",
    method:"POST",
    data:$('#add_form').serialize(),
    beforeSend:function(){
     $('#add_boton').val("Agregando");
    },
    success:function(data){
         $('#add_form')[0].reset();
         $('#add_modal').modal('hide');
         $('#add_boton').val("Agregar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Ingreso con exito");
     }

    });
});
});
</script>
<script type="text/javascript">
//script para enviar el id de correo al delete_modal
	function id_delete(id_pauta){
		$('#input_delete').val(id_pauta);
	}

</script>

<script type="text/javascript">
//ajax para borrar datos de la base de datos
$(document).ready(function(){
      $('#delete_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud_pauta.php",
    method:"POST",
    data:$('#delete_form').serialize(),
    beforeSend:function(){
     $('#btn_delete').val("Borrando");
    },
    success:function(data){
         $('#delete_form')[0].reset();
         $('#delete_modal').modal('hide');
         $('#btn_delete').val("Borrar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Eliminación con exito");
     }

    });
});
});
</script>

<script type="text/javascript">
	//script para enviar los datos al modal update
	function update_data_modal(data){
		d= data.split('||');
		$('#id_pautaU').val(d[0]);
		$('#nombre_pautaU').val(d[1]);
		document.getElementById('tipoU').value(d[2]);

	}
</script>

<script type="text/javascript">
//ajax paa actualizar datos en la base de datos
$(document).ready(function(){
      $('#edit_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud_pauta.php",
    method:"POST",
    data:$('#edit_form').serialize(),
    beforeSend:function(){
     $('#btn_update').val("Actualizando");
    },
    success:function(data){
         $('#edit_form')[0].reset();
         $('#edit_modal').modal('hide');
         $('#btn_update').val("Actualizar");
         $('#btn_delete').val("Borrar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Actualización con exito");
     }

    });
});
});
</script>
<script type="text/javascript">
	//dataTable
	$(document).ready(function() {

    $('#id_tabla').DataTable( {
    	//responsive: true,
    	//"autoWidth": true,
    	"language": {
                "url": "DataTables/Spanish.json"
        }
	} );
} );

</script>
<script>
	$(function(){
  		$("#container-nav").load("navbar.php");
	});
</script>
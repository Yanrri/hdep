<?php 
	session_start();
	require_once("../lib/comun.php");
	require_once '../lib/conexion.php';
	
	$conexion = conectar();
	validarSesion();
	
 ?>
 <!DOCTYPE html>
<html lang="es">
<head>
	<title>HDEP</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="BHost template project">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="styles/bootstrap-4.1.2/bootstrap.min.css">
	<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
	<link href="plugins/video-js/video-js.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="styles/about.css">
	<link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
	<link rel="stylesheet" type="text/css" href="alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="alertifyjs/css/themes/default.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="import" href="navbar.php">
	<link rel="stylesheet" type="text/css" href="DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
</head>
<body>
	<?php 
		$id_modul=$_GET['id_modul'];
		$id_pauta=$_GET['id_pau'];
		$sql_titulo="SELECT nombre_pauta FROM pauta WHERE id_pauta='$id_pauta'";
		$query_titulo=pg_query($conexion,$sql_titulo);
		$titulo=pg_fetch_row($query_titulo);
	 ?>
	<div class="super_container">
	<div id="container-nav">
		</div>
		<!-- Home -->

		<div class="home">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="home_image"><img src="images/about_page.png" alt=""></div>
							<div class="home_title">Pauta: <?php echo ' '.$titulo[0]; ?></div>		
						</div>
					</div>
				</div>
			</div>
			
			<!--<div class="domain_search_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="domain_search">
								<div class="domain_search_background"></div>
								<div class="domain_search_overlay"></div>
								<form action="#" class="domain_search_form" id="domain_search_form">
									<input type="text" class="domain_search_input" placeholder="Filtrar" required="required">
									<button class="domain_search_button">Buscar</button>
								</form>
							</div>
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</div> 
	
	<!-- Pauta -->
	<table class="table table-bordered table-striped" id="tabla_pauta">
		<thead>
			<td></td>
		<?php

			$sqlP="SELECT nombre_pregunta, preg.id_pregunta, count(id_criterio) as numero_de_criterios FROM pregunta as preg JOIN posee as p ON preg.id_pregunta=p.id_pregunta WHERE id_pauta='$id_pauta' GROUP BY preg.id_pregunta ORDER BY numero_pregunta";
			$queryP=pg_query($conexion,$sqlP);
			while($row=pg_fetch_array($queryP)){
				?>
			<td colspan=<?php echo $row[2]; ?> class="text-center text-light" style="background-color: #010146;"><?php echo $row[0]; ?></td>
			<?php 
			}
			?>
		</thead>	
		<tbody>
			<tr>
				<td class="text-center text-dark">Nombre Estudiante</td>
			<?php 
				$sql="SELECT nombre_criterio, puntaje, nombre_pregunta, preg.id_pregunta FROM criterio as c JOIN posee as p ON c.id_criterio=p.id_criterio JOIN pregunta as preg ON p.id_pregunta=preg.id_pregunta WHERE id_pauta='$id_pauta' ORDER BY numero_pregunta,puntaje,nombre_criterio";
				$result=pg_query($conexion,$sql);
				$numero_criterios=0;
				$puntaje_total=0;
				while ($datos=pg_fetch_array($result)){
					?>
					<td class="text-center text-dark"><?php echo $datos["nombre_criterio"]." <br> (". $datos["puntaje"] . " puntos)"; ?></td>
				<?php
					$numero_criterios++;
					$puntaje_total= $puntaje_total + $datos["puntaje"];
				}
			 	?>
			 	<td class="text-center text-dark">Nota</td>
			</tr>
	
			<?php 
				$sql2="SELECT nombre_usuario,porcentaje,obt.id_criterio,nombre_criterio,puntaje,nombre_pregunta,preg.id_pregunta,u.correo_usuario,apellido_usuario FROM pregunta as preg JOIN posee as p ON preg.id_pregunta=p.id_pregunta JOIN criterio as c ON p.id_criterio=c.id_criterio JOIN obtiene as obt ON c.id_criterio=obt.id_criterio AND p.id_pregunta=obt.id_pregunta JOIN usuario as u ON obt.correo_usuario=u.correo_usuario JOIN estudia as e ON u.correo_usuario=e.correo_usuario WHERE id_pauta='$id_pauta' AND id_modulo='$id_modul' ORDER BY apellido_usuario,nombre_usuario,numero_pregunta,puntaje,nombre_criterio";
				$result2=pg_query($conexion,$sql2);
				$num_lines=pg_num_rows($result2);
				//echo $numero_criterios;
				for($i=0;$i<$num_lines;$i++){
					if($i%$numero_criterios==0){
						$suma=0;
						?>
						<tr>
							<td class="text-dark"><?php echo pg_fetch_result($result2, $i, 0).' '. pg_fetch_result($result2,$i,8); ?></td>
					<?php
					}
					?>
							<td class="text-center text-dark"><?php echo $val_obtiene = pg_fetch_result($result2, $i, 1); ?></td>
					<?php
					$puntaje_criterio= pg_fetch_result($result2, $i, 4);
					$suma= $suma + ($val_obtiene * $puntaje_criterio);
					if(($i+1)% $numero_criterios==0){
						?>
						<td class="text-center text-dark"><?php 
						$puntaje_corte=$puntaje_total * 0.6;
						$diferencia=$puntaje_total-$puntaje_corte;
						if($suma<$puntaje_corte){
							$nota=((3*$suma)/$puntaje_corte )+ 1 ;
						}else{
							$nota=((3*($suma-$puntaje_corte))/$diferencia)+4 ;
						}
						echo round($nota,2) ;
						?></td>
						</tr>
					<?php
					}
				}
			 ?>
		</tbody>
	</table>
	<!--Fin Pauta -->




	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="styles/bootstrap-4.1.2/popper.js"></script>
	<script src="styles/bootstrap-4.1.2/bootstrap.min.js"></script>
	<script src="plugins/greensock/TweenMax.min.js"></script>
	<script src="plugins/greensock/TimelineMax.min.js"></script>
	<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="plugins/greensock/animation.gsap.min.js"></script>
	<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="plugins/easing/easing.js"></script>
	<script src="plugins/progressbar/progressbar.min.js"></script>
	<script src="plugins/parallax-js-master/parallax.min.js"></script>
	<script src="plugins/video-js/video.min.js"></script>
	<script src="plugins/video-js/Youtube.min.js"></script>
	<script src="js/about.js"></script>
	<script src="js/importar_html.js"></script>
	<script src="alertifyjs/alertify.js"></script>
	<script src="DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
	<script>
	$(function(){
  		$("#container-nav").load("navbar.php");
	});
</script>
</body>

<?php
//error_reporting(-1);
//ini_set('display_errors', 'true');

session_start();
include '../lib/comun.php';
require_once '../lib/conexion.php';
$conexion = conectar();
validarSesion();


  ?>
<!DOCTYPE html>
<html>
<head>
	<title>HDEP</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="BHost template project">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="styles/bootstrap-4.1.2/bootstrap.min.css">
	<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
	<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
	<link href="plugins/video-js/video-js.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="styles/about.css">
	<!--<link rel="import" href="navbar.php">-->
</head>
<body>
	<div class="container">

		<div id="container-nav">
		</div>
	
</div>
	

	<!-- Home -->
</div>
</div>

	<div class="home">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/1.jpg" data-speed="0.8"></div>
		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content text-center">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Domain Pricing -->

	<div class="domain_pricing">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="domain_pricing_content">
						<ul class="d-flex flex-md-row flex-column align-items-center justify-content-md-between justify-content-center">
							<li><a href="#"><span>.</span>Nombre:<span><?php echo $_SESSION['username'] ;?></span></a></li>
							<li><a href="#"><span>.</span>Correo:<span><?php echo $_SESSION['correo'];
						  ?></span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>	

	<!-- Pricing -->
			
				<?php 
					$otro=$_SESSION['username'];
					$identificar="select id_perfil from usuario where nombre_usuario='$otro'";
					$queri=pg_query($conexion,$identificar);
					$contador=0;
					$nr=pg_num_rows($queri);
					$verificar='';
					
					if($nr>=0){
						while($filas=pg_fetch_array($queri)){
						$verificar = $filas["id_perfil"];
						}
					}
					
					$sql="SELECT nombre_modulo, i.id_modulo FROM usuario as u JOIN imparte as i ON u.correo_usuario=i.correo_usuario JOIN modulo as m ON i.id_modulo=m.id_modulo where nombre_usuario='$otro'  UNION SELECT nombre_modulo, e.id_modulo FROM usuario as u JOIN estudia as e ON u.correo_usuario=e.correo_usuario JOIN modulo as m ON e.id_modulo=m.id_modulo where nombre_usuario='$otro' UNION SELECT nombre_modulo, a.id_modulo FROM usuario as u JOIN ayuda as a ON u.correo_usuario=a.correo_usuario JOIN modulo as m ON a.id_modulo=m.id_modulo where nombre_usuario='$otro'";
				 	$query=pg_query($conexion,$sql);
				 	$contador=0;
				 	if(pg_num_rows($query)>0){
						while(pg_num_rows($query)>$contador){
							$datos= pg_fetch_result($query,$contador,0);
							$datosid= pg_fetch_result($query,$contador,1);
							?>
								<!-- Pricing Item -->
							<ul class="d-flex flex-md-row flex-column align-items-center justify-content-center">
							<div class="row pricing_row">
							<div class="col-lg-4">
								<div class="pricing_item trans_200">
									<div class="pricing_item_background trans_200"></div>
									<div class="pricing_title_container d-flex flex-column align-items-center justify-content-start">
										<div class="pricing_title_background trans_200">
											<svg class="waves" width="100%" height="100%" viewBox="0 0 1920 218" preserveAspectRatio="none">
												<path class="wave_path trans_200" fill="url(#grad_1)" d="M0,0 V210 S500,218 860,193  S1400,125 1920,155 V0 H0"></path>
												<defs>
													<linearGradient id="grad_1">
														<stop offset="0%" stop-color="#487fee" />
														<stop offset="100%" stop-color="#b632fa" />
													</linearGradient>
													<linearGradient id="grad_2">
														<stop offset="0%" stop-color="#9cb9f6" />
														<stop offset="100%" stop-color="#d691fc" />
													</linearGradient>
												</defs>
											</svg>
										</div>
										<div class="pricing_price"><?php echo $datos;  ?><div></div></div>
									</div>
									<div class="pricing_content d-flex flex-column align-items-center justify-content-start">
									<div class="button pricing_button trans_200" id="<?php echo $datosid ?>"><a href="lista_pautas.php?id=<?php echo $datosid;?>"> Seleccionar</a></div>
									</div>
								</div>
							</div>
						</div>
					</ul>

				<?php
						$contador++;
						}
					}
				 ?>
				
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="styles/bootstrap-4.1.2/popper.js"></script>
	<script src="styles/bootstrap-4.1.2/bootstrap.min.js"></script>
	<script src="plugins/greensock/TweenMax.min.js"></script>
	<script src="plugins/greensock/TimelineMax.min.js"></script>
	<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="plugins/greensock/animation.gsap.min.js"></script>
	<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="plugins/easing/easing.js"></script>
	<script src="plugins/progressbar/progressbar.min.js"></script>
	<script src="plugins/parallax-js-master/parallax.min.js"></script>
	<script src="js/custom.js"></script>
	<!--<script src="js/importar_html.js"></script>-->
</div>
</body>
</html>
<script>
	$(function(){
  		$("#container-nav").load("navbar.php");
	});
</script>
<!--<script>
    var link = document.querySelector('link[rel="import"]');

    // Clone the <template> in the import.
    var template = link.import.querySelector('template');
    var clone = document.importNode(template.content, true);

    document.querySelector('#container').appendChild(clone);
 </script>

 -->

<?php 
	session_start();
	require_once("../lib/comun.php");
	require_once '../lib/conexion.php';
	$conexion = conectar();
	validarSesion();
	//una vez que se valida la sesion se debe validar el perfil necesario para acceder a este script
	validarPerfil('1'); // en este caso solamente el administrador puede acceder de lo contario logout
	
 ?>

<!DOCTYPE html>
<html lang="es">
<head>
<title>HDEP</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="BHost template project">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap-4.1.2/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link href="plugins/video-js/video-js.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/about.css">
<link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
<link rel="stylesheet" type="text/css" href="alertifyjs/css/alertify.css">
<link rel="stylesheet" type="text/css" href="alertifyjs/css/themes/default.css">
<link rel="stylesheet" href="styles/icon.css">
<link rel="import" href="navbar.php">
<link rel="stylesheet" type="text/css" href="DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
</head>
<body>

<div class="super_container">
	<div id="container-nav">
		</div>
		<!-- Home -->

		<div class="home">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="home_image"><img src="images/about_page.png" alt=""></div>
							<div class="home_title">Administrar Tipo de Pautas</div>		
						</div>
					</div>
				</div>
			</div>
			
			<!--<div class="domain_search_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="domain_search">
								<div class="domain_search_background"></div>
								<div class="domain_search_overlay"></div>
								<form action="#" class="domain_search_form" id="domain_search_form">
									<input type="text" class="domain_search_input" placeholder="Filtrar" required="required">
									<button class="domain_search_button">Buscar</button>
								</form>
							</div>
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</div> 


	<!-- Modal Agregar -->

	<div id="add_modal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="add_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Agregar Tipo Pauta</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<input id="id_accion1" type="hidden" name="id_accion" value="1"/>					
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" class="form-control" name="nombre_tipo" id="nombre_tipo" required>
						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Agregar" id="add_boton">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Fin modal add >

	<-- Modal delete -->	
	<div id="delete_modal" class="modal fade">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<form id="delete_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Borrar Usuario</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>¿Está seguro que desea borrar este registro?</p>
						<p class="text-warning"><small>Esta acción no se puede deshacer</small></p>
					</div>
					<div class="modal-footer">
						<input id="input_delete" type="hidden" name="id_tipoD" value=""/>
						<input id="id_accion3" type="hidden" name="id_accion" value="3"/>
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-danger" id="btn_delete" value="Borrar">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Fin modal delete > 

	<-- Modal Update-->
	<div id="edit_modal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="edit_form" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Editar Tipo de Pauta</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" class="form-control" name="nombre_tipoU" id="nombre_tipoU" required>
							<input type="hidden" name="id_tipoU" id="id_tipoU">
						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-info" value="Actualizar" id="btn_update">
					</div>
				</form>
			</div>
		</div>
	</div>
	<br><br><br>

		<div class="col-4 align-self-start">
			<button class="domain_search_button" data-toggle="modal" data-target="#add_modal">+ Agregar</button>
		</div>

	
	<!-- Team -->
		<div class="container" id="container_tabla">
			<div class="section_title_container text-center">
				<div class="row team_row">
					<div class="team_text"></div>
	                <div class="table-responsive">
	                <table class="table" id="id_tabla" >
	                    <thead>
	                        <tr>
	                            <th class="text-center">#</th>
	                            <th>Nombre</th>
	                            <th>Acciones</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php
	                    	$sql="SELECT id_tipo, nombre_tipo FROM tipo ORDER BY nombre_tipo";
	                    	$result=pg_query($conexion,$sql);
	                    	$contador=1;
	                    	while ($datos=pg_fetch_row($result)) {
	                    		$update= $datos[0]."||".$datos[1];
	                    	?>
	                    		<tr>
		                            <td class="text-center"><?php echo $contador; ?></td>
		                            <td><?php echo $datos[1]; ?></td>
		                            <td>
		                            	<a href="#edit_modal" class="text-warning" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Editar" onclick="update_data_modal('<?php echo $update; ?>')">&#xE254;</i></a>
	                            		<a href="#delete_modal" class="text-danger" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Borrar" onclick="id_delete('<?php echo $datos[0]; ?>')">&#xE872;</i></a>
	                            	</td>
		                            <!--<td class="center">
		                                <input type="button" name="Participantes" value="Detalles" id="<?php echo $datos[2]; ?>" class="btn btn-info btn-xs view_data"/>
	                            	</td>-->
	                        	</tr>
	                    	<?php
	                    		$contador++;	
	                    	}
	                    	?>
	                    </tbody>
	                </table>
	                </div>
                </div>
            </div>
        </div>
        			<div class="modal fade" id="participantes_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-header text-center">
		        <h4 class="modal-title w-100" id="exampleModalLabel">Modulos</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body" id="detalles_modulo">

	        	<!-- Tabla modal -->
	        	<!--<div class="container">
	        	</div>-->
	        	<div id="id_tabla_modal"></div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
		      </div>
		    </div>
		  </div>
		</div>
	</div>

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="styles/bootstrap-4.1.2/popper.js"></script>
	<script src="styles/bootstrap-4.1.2/bootstrap.min.js"></script>
	<script src="plugins/greensock/TweenMax.min.js"></script>
	<script src="plugins/greensock/TimelineMax.min.js"></script>
	<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="plugins/greensock/animation.gsap.min.js"></script>
	<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="plugins/easing/easing.js"></script>
	<script src="plugins/progressbar/progressbar.min.js"></script>
	<script src="plugins/parallax-js-master/parallax.min.js"></script>
	<script src="plugins/video-js/video.min.js"></script>
	<script src="plugins/video-js/Youtube.min.js"></script>
	<script src="js/about.js"></script>
	<script src="js/importar_html.js"></script>
	<script src="alertifyjs/alertify.js"></script>
	<script src="DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
</body>
</html>
<script type="text/javascript">
	//ajax para enviar el id y crear la tabla dentro del modal
	$(document).ready(function(){
		$('.view_data').click(function(){
			var pregunta_id= $(this).attr("id");
			$.ajax({
				url:"view_modulos.php",
				method:"post",
				async:false,
				data:{pregunta_id:pregunta_id},
				success:function(data){
					$('#detalles_modulo').html(data);
					$('#id_moduloU').val(pregunta_id);
					$('#participantes_modal').modal("show");
					/*$('#id_tabla_m').DataTable( {
				    	//responsive: true,
				    	//"autoWidth": true,
				    	"language": {
				                "url": "DataTables/Spanish.json"
				        }
					} );*/
				}
			});
		});
	});
</script>

<script type="text/javascript">
//ajax para agregar a la base de datos
$(document).ready(function(){
      $('#add_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud.php",
    method:"POST",
    data:$('#add_form').serialize(),
    beforeSend:function(){
     $('#add_boton').val("Agregando");
    },
    success:function(data){
         $('#add_form')[0].reset();
         $('#add_modal').modal('hide');
         $('#add_boton').val("Agregar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Ingreso con exito");
     }

    });
});
});
</script>
<script type="text/javascript">
//script para enviar el id de correo al delete_modal
	function id_delete(correo){
		$('#input_delete').val(correo);
	}

</script>

<script type="text/javascript">
//ajax para borrar datos de la base de datos
$(document).ready(function(){
      $('#delete_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud.php",
    method:"POST",
    data:$('#delete_form').serialize(),
    beforeSend:function(){
     $('#btn_delete').val("Borrando");
    },
    success:function(data){
         $('#delete_form')[0].reset();
         $('#delete_modal').modal('hide');
         $('#btn_delete').val("Borrar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Eliminación con exito");
     }

    });
});
});
</script>

<script type="text/javascript">
	//script para enviar los datos al modal update
	function update_data_modal(data){
		d= data.split('||');

		$('#nombre_usuarioU').val(d[0]);
		$('#correo_usuarioU').val(d[1]);
		$('#passwordU').val(d[2]);
		if(d[3]== 2){
			$("#perfil2U").attr('checked', true);
		}if(d[3]==3){
			$("#perfil3U").attr('checked', true);
		}if(d[3]==4){
			$("#perfil4U").attr('checked', true);
		}

	}
</script>

<script type="text/javascript">
//ajax paa actualizar datos en la base de datos
$(document).ready(function(){
      $('#edit_form').on("submit", function(event){
  event.preventDefault();
     $.ajax({
    url:"crud.php",
    method:"POST",
    data:$('#edit_form').serialize(),
    beforeSend:function(){
     $('#btn_update').val("Actualizando");
    },
    success:function(data){
         $('#edit_form')[0].reset();
         $('#edit_modal').modal('hide');
         $('#btn_update').val("Actualizar");
         $('#btn_delete').val("Borrar");
         $('#id_tabla').html(data);
         $('#id_tabla').load(location.href+" #id_tabla>*","");
         alertify.success("Actualización con exito");
     }

    });
});
});
</script>
<script type="text/javascript">
	//dataTable
	$(document).ready(function() {

    $('#id_tabla').DataTable( {
    	//responsive: true,
    	//"autoWidth": true,
    	"language": {
                "url": "DataTables/Spanish.json"
        }
	} );
} );

</script>
<script>
	$(function(){
  		$("#container-nav").load("navbar.php");
	});
</script>
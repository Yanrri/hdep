<?php

if (session_status() == PHP_SESSION_NONE) {
  session_start();
}

setlocale (LC_TIME, "es_ES.utf8");

function validarSesion() {
  if (isset($_SESSION["loggedin"])) {
    if (!$_SESSION["loggedin"] == true) {
      header('Location: ../lib/logout.php');
    }
  } else {
    header('Location: ../lib/logout.php');
  }
  //
}

function validarPerfil($perfil){
	if (isset($_SESSION['perfil'])){
		if($_SESSION['perfil']!==$perfil){
  			header('Location: ../lib/logout.php');
  		}
  	}else{
  		header('Location: ../lib/logout.php');
  	}
}

/*
 * devuelve ID conexión a la DB
 */
function conectarBD () {
  $conexion=pg_connect("host=localhost dbname=hdep3 user=pautix password=holamundo")
    		or die("No se puede conectar a la Base de Datos".pg_last_error($conexion));
    	return $conexion;
}

/*function ejecutarSQL ($stmt) {
  $res = array();
  $res["success"] = false;
  $res["msg"] = "Error SQL";
  $res["data"] = null;

  try {
    if ($stmt->execute()) {
      $res["data"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $res["msg"] = "éxito";
      $res["success"] = true;
    } else {
      //http://php.net/manual/en/pdostatement.errorinfo.php
      $res["msg"] = $stmt->errorInfo();
      //$res["msg"] = "Error SQL";
    }

  } catch (PDOException $e) {
    $res["msg"] = $e->getMessage();
  }

  return $res;
}*/


/*function head() {

	$str = <<<EOF
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="hdep">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="../extras/bootstrap4/css/bootstrap.css">
	EOF;
	echo $str;
}

function hacer_navbar(){
	$strIn = <<<EOF
	<header class="header trans_400">
			<div class="header_content d-flex flex-row align-items-center justify-content-start trans_400">
				<div class="logo"><a href="#"><span>b</span>Host</a></div>
				<div class="container">
					<div class="row">
						<div class="col-lg-10 offset-lg-2">
							<nav class="main_nav">
								<ul class="d-flex flex-row align-items-center justify-content-start">
									<li class="active"><a href="index.html">Home</a></li>
									<li><a href="about.html">About us</a></li>
									<li><a href="services.html">Services</a></li>
									<li><a href="blog.html">News</a></li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
				<div class="header_right d-flex flex-row align-items-center justify-content-start">

					<!-- Header Links -->
					<div class="header_links">
						<ul class="d-flex flex-row align-items-center justify-content-start">
							<li><a href="#">Webmail</a></li>
							<li><a href="#">Chat</a></li>
							<li><a href="#">Login</a></li>
						</ul>
					</div>

					<!-- Phone -->
					<div class="phone d-flex flex-row align-items-center justify-content-start">
						<i class="fa fa-phone" aria-hidden="true"></i>
						<div>652-345 3222 11</div>
					</div>

					<!-- Hamburger -->
					<div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
				</div>	
			</div>
		</header>

		<!-- Menu -->

		<div class="menu trans_500">
			<div class="menu_content d-flex flex-column align-items-center justify-content-center">
				<div class="menu_nav trans_500">
					<ul class="text-center">
						<li><a href="index.html">Home</a></li>
						<li><a href="about.html">About us</a></li>
						<li><a href="services.html">Services</a></li>
						<li><a href="blog.html">News</a></li>
						<li><a href="contact.html">Contact</a></li>
					</ul>
				</div>
				<div class="phone menu_phone d-flex flex-row align-items-center justify-content-start">
					<i class="fa fa-phone" aria-hidden="true"></i>
					<div>652-345 3222 11</div>
				</div>
			</div>
		</div>
		EOF;
}

function footer() {
$str = <<<EOF
<hr/>
<div class="text-center" id="foot-contact">
  <a href="{$GLOBALS['ini']['url']}">{$GLOBALS['ini']['nombre']} - {$GLOBALS['ini']['url']}</a>
  | <a href="mailto: {$GLOBALS['ini']['contacto']}">{$GLOBALS['ini']['contacto']}</a>
</div>

<script>window.jQuery || document.write('<script src="../js/vendor/jquery-3.3.1.js"><\/script>')</script>
<script src="../js/vendor/tether.min.js"></script>
<script src="../js/vendor/bootstrap.min.js"></script>
<link rel="stylesheet" href="../css/jquery.dataTables.css">
<script src="../js/vendor/sweetalert2/dist/sweetalert2.all.min.js"></script>
<link rel="stylesheet" href="../js/vendor/sweetalert2/dist/sweetalert2.min.css">

EOF;
echo $str;
}*/

?>

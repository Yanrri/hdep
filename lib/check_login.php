<?php
session_start();
require_once '../lib/comun.php';
require_once '../lib/conexion.php';
$conexion = conectar();

$username = $_REQUEST['username'];
$pwd = $_REQUEST['pwd'];

$sql="SELECT nombre_usuario,correo_usuario, password_usuario, id_perfil FROM usuario WHERE correo_usuario='$username'";
$query= pg_query($conexion,$sql);

if(pg_num_rows($query)>0) {
	$datos=pg_fetch_array($query);
	if(password_verify($pwd,$datos[2])) {
		$_SESSION['username']= $datos[0];
		$_SESSION['correo']= $datos[1];
		$_SESSION['perfil']=$datos[3];
		$_SESSION['loggedin']= true;
		$success = true;
		$msg="ok";
		$location = "../main/index.php";
	}else{
		$_SESSION['loggedin'] = false;
	  	$success = false;
	  	$msg = "Error de usuario o clave";
	  	$location = "../lib/logout.php";
	}
}else {
  $_SESSION['loggedin'] = false;
  $success = false;
  $msg = "Error de usuario o clave";
  $location = "../lib/logout.php";
}


$jsonOutput = array('success' => $success, 'msg' => $msg, 'location'=> $location);
echo json_encode($jsonOutput);
?>
